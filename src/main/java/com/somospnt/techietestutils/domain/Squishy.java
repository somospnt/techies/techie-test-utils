package com.somospnt.techietestutils.domain;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Squishy {

    private String nombre;
    private String precio;
    private String apretabilidad;
}
