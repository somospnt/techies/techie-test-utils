package com.somospnt.techietestutils.service;

import com.somospnt.techietestutils.domain.Squishy;
import com.somospnt.techietestutils.repository.*;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SquishyService {

    @Autowired
    private SquishyRepository squishyRepository;

    public List<Squishy> obtenerPorNombres(List<String> nombres) {
        return nombres.stream()
                .map(nombre -> squishyRepository.obtenerPorNombre(nombre))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
    }

}
