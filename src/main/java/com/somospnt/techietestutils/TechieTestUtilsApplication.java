package com.somospnt.techietestutils;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TechieTestUtilsApplication {

	public static void main(String[] args) {
		SpringApplication.run(TechieTestUtilsApplication.class, args);
	}
}
